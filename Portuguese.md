## Competências

\fontsize{5}{6}\selectfont

|            |          |              |            |               |                     |
| ---------- | -------- | ------------ | ---------- | ------------- | ------------------- |
| Leadership | Teamwork | Adaptability | Gumption   | Sharing       | ICT monitoring      |
| Python     | DevOps   | Datacenter   | Kanban     | Scrum         | C4 Model            |
| HA         | Cluster  | SonarQube    | KPI        | CEntOS/RedHat | Fedora/Debian       |
| Git        | Gitlab   | Gitlab CI    | Codefresh  | CI/CD         | Datadog             |
| Docker     | Rancher  | Kubernetes   | Containers | Vagrant       | Business Continuity |

\normalsize

# Sobre mim

Entusiasta da integração de equipes e agilidade na entrega de produtos e resultados, atua também como agente influenciador na implantação de infraestruturas ágeis e na internalização de práticas Lean & DevOps.
Profissional de TIC com mais de 20 anos de experiência trabalhando e gerenciando equipes em ambientes de Datacenter, tendo atuado na concepção, análise e desenvolvimento de sistemas de baixa a alta complexidade como ERP.
Já atuou como líder de desenvolvimento de projetos de código aberto (CACIC[^cacic]) e contribui para projetos dessa natureza[^gitlab] [^github].
É Formado em Engenharia Mecânica e possui pós-graduação MBA Gestão de Negócios.

[^cacic]: Configurador Automático e Coletor de Informações Computacionais ([https://softwarepublico.gov.br/gitlab/groups/cacic](https://softwarepublico.gov.br/gitlab/groups/cacic))
[^gitlab]: [https://gitlab.com/adrianovieira](https://gitlab.com/adrianovieira)
[^github]: [https://github.com/adrianovieira](https://github.com/adrianovieira)

# EXPERIÊNCIA PROFISSIONAL

**Tech Lead [RadixEng](http://www.radixeng.com.br)** –
setembro/2022 - novembro/2024 – (remoto)

- Design arquitetônico em padrões de microsserviços em camadas e orientados a eventos aplicando a metodologia do Modelo C4 para clientes nos setores de serviços e industrial.
- Supervisão de engenheiros, testadores e membros da equipe DevOps em modo de trabalho remoto.
- Mentoria de colegas de trabalho para melhorar seu conhecimento prático e planejamento de carreira.
- Envolvimento direto em análise, revisão e desenvolvimento de sistemas full-stack em Python-3.10/Fastapi/Pydantic e Typescript/React integrados ao CIAM Auth0.
- Desenvolvimento de backend API sob Python aplicando padrão de design de arquitetura hexagonal.
- Refatoração do módulo Repositório de backend API Python, que reduziu enfileiramento de requisições do banco de dados a zero.
- Desenvolvimento de FaaS a serem implantadas no AWS Lambda e Azure Functions para integrar o sistema CIAM Auth0, a aplicação Web/React e API Python tornando o acesso mais seguro aos serviços.
- Configuração da API de backend para ser usada com segurança via AWS Congnito usando o protocolo oAuth2.
- Implementação de conectores no Kafka Connect para transmitir dados para AWS RDS e AWS Aurora.
- Desenvolvimento de produtores e consumidores em Python, integrando-os com tópicos Kafka para enviar/obter dados para análise de analistas de negócios e proprietários de produtos no PowerBI.
- Documentação de sistemas sob o padrão OpenAPI-3.0 com a ferramenta Stoplight Studio e Mule Desing/Exchange.
- Desenvolvimento de testes integrados usando Pytest (backend) e Vitest (frontend), resultando em 80% de cobertura (via Sonarqube).
- Responsável por organizar o desenvolvimento e manutenção de sistemas de produção usando Atlassian Jira (kanban/Scrum) e serviços Datadog.
- Liderou uma média de 5 membros da equipe de engenharia de sistemas usando as melhores práticas do setor para desenvolvimento de software, qualidade de código e testes.
- Responsável por projetar microsserviços Python e React/Typescript.
- Desenvolvimento de microsserviços Python integrados a PostgreSQL, Kafka, Redis, Auth0 e Mule Gateway como serviços de backend.
- Suporte à equipe de engenharia no padrão de design de arquitetura hexagonal para desenvolvimento de microsserviços.
- Desenvolvimento de casos de teste de sistemas suportados por ferramentas como Codefresh-CI, SonarQube, pyTest, Playwright, Locust e K6.
- Responsável por organizar o desenvolvimento e manutenção de sistemas de produção no AWS Fargate, FaaS/Lambda.
- Diretamente envolvido na integração de serviços como WEB, API, RDS/Database, FaaS/Lambda, Kafka, Codefresh, Datadog, Mule Gateway e Auth0.

**Tech Lead, [Zup IT Innovation](http://www.zup.com.br)** –
outubro/2021 - agosto/2022 – (remoto)

- Tech Lead do projeto Beagle BFF ([https://usebeagle.io](https://usebeagle.io)).
- Tech Lead do projeto RitchieCLI noOps ([https://ritchiecli.io](https://ritchiecli.io)).
- Supervisão de equipe em modo trabalho remoto.
- Mentoria de colaboradores para aprimoramento de conhecimento e planejamento de carreira.

**Tech Manager NodeJS, DevOps, [Empresa Brasil de Comunicação](http://www.ebc.com.br)** –
outubro/2020 - setembro/2021 – Brasília/DF, Brasil

- Desenho e suporte em definição de arquitetura de sistemas implementados em Java, Javascript, Python
- Responsável por desenho de sistemas em NodeJS/Javascript e Python
- Desenvolvimento full-stack de sistema em SPA/VueJS frontend, API/backend em NodeJS/Javascript e ORM/Sequelize, persistência em SGBD PostgreSQL
- Responsável por coordenar a integração do desenvolvimento de sistemas e infraestrutura com suporte a revisão de arquiteturas de implantação, testes estáticos e funcionas
- Desenvolvimento de testes sistemas com suporte de ferramentas como _Insomnia_, _Gitlab/CI_, _SonarQube_, _Mocha/ChaiJS_, _Cypress_
- Diretamente envolvido na integração de sistemas com serviço de chat via desenvolvimento de _bots_ ou _webhooks_ em Javascript
- Suporte a equipes de desenvolvedores (Java, Javascript) em diagnóstico e resolução de anomalias nos sistemas e integrações existentes
- Responsável por estruturar portal de documentação e gerar conteúdos referentes a padronizações de desenvolvimento sistemas e automação de infraestrutura com uso de _asciidoc_
- Responsável por documentar sistema sobe padrão _OpenAPI_ com suporte da ferramenta _Stoplight Studio_
- Suporte ao desenho e desenvolvimento de códigos para automação implantação de infraestrutura (_InfraAsCode_)
- Orientação e implementação de _dashboards_ no serviço de painéis de gestão sob _Grafana_ consumindo dados de _Elasticsearch_, Bancos SGBD e de monitoramento de serviços
- Propor e patrocinar melhorias de processos de gestão de desenvolvimento de sistemas e integrações com infraestrutura
- Diretamente envolvido no desenho de arquitetura de implantação de serviço de painéis de gestão para apoio à tomada de decisões
- Supervisão de equipe em modo trabalho remoto
- Mentoring de colaboradores para aprimoramento de conhecimento e planejamento de carreira usando a produção conteúdo em _blog_ como exemplo de ferramenta de suporte

**Tech Manager NodeJS, Python, DevOps, [Empresa Brasil de Comunicação](http://www.ebc.com.br)** –
setembro/2018 - outubro/2020 – Brasília/DF, Brasil

- Responsável por coordenar o desenvolvimento e manutenção de sistemas de produção
- Liderar equipe de desenvolvimento de sistemas usando práticas de mercado para qualidade de código e testes
- Desenho e suporte em definição de arquitetura de sistemas implementados em Java, Javascript, Python
- Desenvolvimento de sistemas usando linguagens Javascript, Python, Go Lang
- Responsável por desenho de sistemas em Python
- Desenvolvimento de sistema em Python com persistência em SGBD PostgreSQL e NoSQL/Elasticsearch, e criação de dashboards em Kibana ou Grafana
- Responsável pelo desenho de sistema de visão computacional em Python para análise de conteúdo em _broadcasting_ de TV
- Diretamente envolvido na análise e desenvolvimento de sistema de visão computacional em Python para análise de conteúdo em _broadcasting_ de TV
- Diretamente envolvido na análise e desenvolvimento _fullstack_ de sistemas referenciando melhores práticas e métodos de _tests/QA_
- Desenvolvimento de testes sistemas com suporte de ferramentas como _Insomnia_, _Gitlab/CI_, _SonarQube_, _Mocha/ChaiJS_, _Cypress_
- Orientação de equipes de desenvolvimento para implementação de _CI/CD_, _tests/QA_, _deploy_ automatizado de sistemas
- Orientação e suporte a equipes na implementação de _pipeline_ para o ciclo completo da aplicação (exemplo: _test/QA_, _release_, _deploy_) e com uso de ferramentas como Gitlab/CI, Sonatype Nexus, Rancher/Kubernetes, Ansible
- Suporte ao desenho e desenvolvimento de códigos para automação implantação de infraestrutura (_InfraAsCode_)
- Gestão de desenvolvimento de automação de operações de infraestrutura e com suporte de ferramentas como _Ansible_, _Kubernetes_, _Helm Chart_
- Gestão de operações de clusteres _Kubernetes como Serviço_ com uso de ferramentas _Rancher_ e _Kubernetes_
- Supervisão de equipe em modo trabalho remoto
- Mentoring de colaboradores para aprimoramento de conhecimento e planejamento de carreira usando _Dojo/Kata_ como de ferramenta de suporte

**Assessor para Arquitetura e Planejamento de Serviços de Infraestrutura de TIC, [Dataprev](http://www.dataprev.gov.br)** – maio/2010 a junho/2018 – Brasília/DF

- Supervisão de equipes locais e remotas no desenvolvimento de sistemas ou serviços construindo _pipeline_ de automação para testes de aplicação ou módulos de infraestrutura em código;
- Assessoria em contratação de suporte/serviços;
- Supervisão de times locais e remotos para implementação de datalake usando o framework Hadoop e ferramentas analíticas de consumo que propiciou adicionar o serviço GovData brasileiro ao catálogo disponibilizado para clientes governamentais;
- Assessoria em boas práticas de segurança da informação na implementação de plano de continuidade e gerenciamento de risco em TIC, boas práticas para assegurar a qualidade e disponibilidade de serviços;
- Assessoria em melhoria de processos como o de gestão de incidentes e de monitoramento de sistemas e serviços;
- Responsável por gestão de projetos internos e em parceria entre empresas públicas ou privadas;
- Assessoramento para construção de Plano Diretor de TIC;
- Supervisão de projetos de implementação de serviços e tecnologias como git/gitlab, gitlab-ci, jenkins-ci, puppet, docker, rancher
- Responsável por guiar equipes em desenho e implementação de monitoramento e testes de serviços via CA Suite, zabbix, browser dev tools/firebug/page speed.

**Gerente de Operações de TI, [Politec -> Indra Company](http://www.indracompany.com)** – setembro/2006 a abril/2010 – Brasília/DF

- Gerência de equipe de TI em operação de sistemas e serviços para usuários internos ao Ministério do Trabalho e aos SINE, bem como para o cidadão;
- Desenvolvimento e implementação de serviço para automação de _deploy_ contínuo de aplicações;
- Assessoria em Contratação de Suporte/Serviços;
- Assessoria em definições de políticas de proteção de dados, metodologia de desenvolvimento de sistemas, boas práticas de segurança da informação, boas práticas para assegurar a qualidade e disponibilidade de serviços;
- Administração de plataforma Linux (CentOS/Redhat, Debian) e Windows (2000 e 2003);
- Administração de infra-estrutura de máquinas virtuais (Linux e Windows) em VMWare Server e VMWare ESXi;
- Administração e suporte a servidores (alguns clusterizados) de aplicações Apache, Jboss-4, IIS5, IIS6;
- Administração, suporte e treinamento em gestão de configuração e de mudanças em serviços;
- Implantação de sistemas de Gestão de Ativos e Software de TI;
- Administração de servidores DELL-2950, IBM-236/346/445, DELL-6800 e HP-ML370.

### Outras experiências

- **Consultor de TI, Autônomo** - Fevereiro/2004-Janeiro/2007 - MG/Brasil
- **Engenheiro de Sistemas, [Aethra Componentes Automotivos](http://www.aethra.com.br)** - Julho/1992-Outubro/2005 - Betim, MG/Brasil
- **Desenvolvedor, [Micro Universo Informática](http://www.microuniverso.com.br)** - Setembro/1990-Junho/1992 - Belo Horizonte, MG/Brasil
- **Desenvolvedor, Vitória Passos Informática** - Julho/1989-Agosto/1990 - Belo Horizonte, MG/Brasil

# EVENTOS

Obtive oportunidade de conduzir fórum/conferências/palestras abertas ao público em geral, entre as quais:

- **2º DevOpsDays Brasília**, 2017 – membro da equipe organizadora, Brasília/DF, Brasil
- **Open Networking Summit 2017**, 2017 – Panel Discussion: CI/CD Lessons Learned in Open Source Networking - Painelista, Santa Clara/CA, EUA
- **1º DevOpsDays Brasília**, 2016 – membro da equipe organizadora; palestrante em open-space _Gitlab-CI+Rancher_, Brasília/DF, Brasil
- **58º Fórum de TIC da Dataprev**, 2014 – Soluções de Big Data – membro de equipe organizadora, Brasília/DF, Brasil
- **56º Fórum de TIC da Dataprev**, 2014 – DevOps: Comunicação, colaboração e integração para a entrega contínua e operações estáveis em TIC – membro de equipe organizadora, Brasília/DF, Brasil
- **10º Fórum Internacional do Software Livre (FISL 10.0)**, 2009 – Palestra para a Comunidade CACIC, Porto Alegre/RS, Brasil

# PRÊMIOS GANHOS

- **I Prêmio Ação Coletiva - FISL 9.0** - por Associação do Software Livre e Ministério do Planejamento Orçamento e Gestão (SLTI)
- **II Prêmio Ação Coletiva - Latinoware 2008** - por Fundação Parque Tecnológico Itaipu e Ministério do Planejamento Orçamento e Gestão (SLTI)
- **III Prêmio Ação Coletiva – Convenção SPB 2009** - por ATA-Associação de Tecnologias Abertas (ATA), Intel® e Portal de Software Público Brasileiro (SPB)

# CURSOS EXTRA-CURRICULARES

- **Big Data 101** - por Cognitive Class (<https://courses.cognitiveclass.ai/certificates/c84511fa6a0d4c3c8284abb562a5518c>) – janeiro/2018
- **Hadoop 101** - por Cognitive Class (<https://courses.cognitiveclass.ai/certificates/a5854b8c96d6486596a2da37771dcbbc>) – janeiro/2018
- **Direito Digital** – por PPP Advogados – 30h – setembro/2016
- Gestão de Projetos, COBIT, ITIL, Sistemas multi-agentes, Windows Server, Linux Redhat e SUSE Linux

# CERTIFICAÇÕES

- **ITIL Foundation** – por Prometric/EXIN, Registro \#: SA 7LAM 5005

# IDIOMAS

- **Português** – Natural
- **Inglês** – Avançado
- **Espanhol** – Básico

# FORMAÇÃO ACADÊMICA

- Especialização em Engenharia de Software – Universidade Federal de Minas Gerais – incompleto (2006)
- MBA Gestão Empresarial – Fundação Getúlio Vargas/RJ – (2004)
- Graduação Engenharia Mecânica – Pontifícia Universidade Católica de Minas Gerais – (2000)

# **ATIVIDADES ACADÊMICAS / TREINAMENTO / VOLUNTARIADO**

- Comitê Consultivo para melhoria do _Portal do Software Público Brasileiro_, Membro do Comitê – 2009
- [Projeto CACIC](https://softwarepublico.gov.br/social/cacic) – _ferramenta de código aberto para inventário de TIC_, Desenvolvedor Líder – de abril/2007 a abril/2010
- Projeto social de inclusão digital e social – 2005/2006 – Camargos/BH – trabalho voluntário
- Ministração de palestras, cursos e treinamentos a Gerentes, Supervisores, Gestores, Analistas de Suporte e outros usuários, bem como à comunidade em geral – 1995/2004; Módulos ERP – Contabilidade, Finanças, Produção, Comercial, Compras – 1993/2004;
- Autoria de artigo: [Revista Espírito Livre](http://www.revista.espiritolivre.org), ed 10, disponível em: <http://tiny.cc/EspiritoLivreMagazine>
- Contribuições e compartilhamento de código:
  - Gitlab: [https://gitlab.com/adrianovieira](https://gitlab.com/adrianovieira/)
  - Github: <https://github.com/adrianovieira>
  - Vagrant Boxes: <https://app.vagrantup.com/adrianovieira>; <https://app.vagrantup.com/entlinux>
  - Docker images: <https://hub.docker.com/r/adrianovieira/>
  - Software Público Brasileiro (harpiain): [https://softwarepublico.gov.br/gitlab/cacic](https://softwarepublico.gov.br/gitlab/cacic)
  - Speakerdeck: [https://speakerdeck.com/adrianovieira](https://speakerdeck.com/adrianovieira)
  - Sourceforge (harpiain): [https://sourceforge.net/u/harpiain](https://sourceforge.net/u/harpiain/)
  - Bitbucket: [https://bitbucket.org/adriano_svieira/](https://bitbucket.org/adriano_svieira/)
  - blog: [https://adrianovieira.gitlab.io](https://adrianovieira.gitlab.io)
