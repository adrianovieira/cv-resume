if [[ "$1" == "en" ]]; then
  CV_FILE=English.md
else
  CV_FILE=Portuguese.md
fi
pandoc -o adrianovieira-$1.pdf resume-metadata.md $CV_FILE --template=scripts/template.tex --csl=scripts/resume.cls
