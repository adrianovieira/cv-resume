## Skills

\fontsize{7}{8}\selectfont

|            |          |              |            |               |                     |
| ---------- | -------- | ------------ | ---------- | ------------- | ------------------- |
| Leadership | Teamwork | Adaptability | Gumption   | Sharing       | ICT monitoring      |
| Python     | DevOps   | Datacenter   | Kanban     | Scrum         | C4 Model            |
| HA         | Cluster  | SonarQube    | KPI        | CEntOS/RedHat | Fedora/Debian       |
| Git        | Gitlab   | Gitlab CI    | Codefresh  | CI/CD         | Datadog             |
| Docker     | Rancher  | Kubernetes   | Containers | Vagrant       | Business Continuity |

\normalsize

# About me

Enthusiastic in team integration and agility in delivering products, he acts as an influencing agent in the implementation of agile infrastructures and in the internalization of Lean & DevOps practices.
He is an ICT professional with over 20 years of experience working and managing teams in Datacenter environments, having worked in the design, analysis, and development of low to high complexity systems such as ERP. He has also acted as the lead of open-source project development (CACIC[^cacic]) and contributes to projects of this nature[^gitlab] [^github].
a^github].
He holds a degree in mechanical engineering and an MBA Business Management.

[^cacic]: Configurador Automático e Coletor de Informações Computacionais ([https://softwarepublico.gov.br/gitlab/groups/cacic](https://softwarepublico.gov.br/gitlab/groups/cacic))
[^gitlab]: [https://gitlab.com/adrianovieira](https://gitlab.com/adrianovieira)
[^github]: [https://github.com/adrianovieira](https://github.com/adrianovieira)

# WORK EXPERIENCE

**Tech Lead [Radix Engenharia e Software](http://www.radixeng.com.br)** –
Sep/2022 - Nov/2024 – (remote)

- Architectural design in layered and event-driven microservices patterns applying C4 Model methodology for clients in the service and industrial sectors.
- Supervision of engineers, testers and DevOps team members in remote work mode.
- Mentoring coworkers to improve their working knowledge and career planning.
- Directly involved in full-stack systems analysis, review and development in Python-3.10/Fastapi/Pydantic and Typescript/React integrated into CIAM Auth0.
- Backend development of Python microservices applying hexagonal architecture design pattern.
- Refactored the Repository module of a backend Python API, which reduced database queued requests to zero.
- Development of FaaS to be deployed on AWS Lambda and Azure Functions to integrate the CIAM Auth0 system, the Web/React application and Python API, making access to services more secure.
- Configured backend API to be securely used via AWS Congnito using oAuth2 protocol.
- Implemented connectors on Kafka Connect to stream data to AWS RDS and AWS Aurora.
- Developed producers and consumers in Python, integrating them with Kafka topics to send/get data for business analysts' and product owners' analysis in PowerBI.
- Documented system under OpenAPI-3.0 standard with Stoplight Studio tool and Mule Desing/Exchange.
- Developed integrated tests using Pytest (backend) and Vitest (frontend), resulting in 80% coverage (via Sonarqube).
- Responsible for organizing the development and maintenance of production systems using Atlassian Jira (kanban/Scrum) and Datadog services.
- Led an average of 5 systems engineering team members using industry best practices for software development, code quality, and testing.
- Responsible for designing Python and React/Typescript microservices.
- Development of Python microservices integrated into PostgreSQL, Kafka, Redis, Auth0, and Mule Gateway as backend services.
- Supporting engineering team in hexagonal architecture design pattern for microservices development.
- Development of systems test cases supported by tools such as Codefresh-CI, SonarQube, pyTest, Playwright, Locust, and K6.
- Responsible for organizing the development and maintenance of production systems on AWS Fargate, FaaS/Lambda.
- Directly involved in integrating services like WEB, API, RDS/Database, FaaS/Lambda, Kafka, Codefresh, Datadog, Mule Gateway, and Auth0.

**Tech Lead, [Zup IT Innovation](http://www.zup.com.br)** –
Oct/2021 - Aug/2022 – (Remote)

- Team supervision in remote work mode
- Mentoring of employees to improve knowledge and career planning
- Tech Lead Beagle BFF project ([https://usebeagle.io](https://usebeagle.io))
- Tech Lead RitchieCLI noOps project ([https://ritchiecli.io](https://ritchiecli.io))

**Tech Manager NodeJS, DevOps, [Empresa Brasil de Comunicação](http://www.ebc.com.br)** –
Oct/2020 – Sep/2021 – Brasília/DF, Brazil

- Design and support in determining the architecture of systems implemented in Java, Javascript, Python
- Responsible for coordinating the integration of both systems' development and infrastructure, reviewing deployment architectures, static and functional tests
- Responsible for designing NodeJS/Javascript systems
- Full-stack development of system using SPA/VueJS as frontend, NodeJS/Javascript API as backend, and ORM/Sequelize on PostgreSQL
- Development of systems tests case supported by tools such as Insomnia, Gitlab/CI, SonarQube, Mocha/ChaiJS, Cypress
- Directly involved in systems' integration with chat service via the development of Javascript bots or webhooks
- Support developers teams (Java, Javascript) in diagnosing and solving anomalies in living systems and integrations
- Accountable for structuring documentation portal and generating content related to systems' development standards and infrastructure automation using asciidoc
- Accountable for documenting system under OpenAPI standard with Stoplight Studio tool support
- Assist in the design and development of automation codes for infrastructure deployment (InfraAsCode)
- Guidance and implementation of support decision panels dashboards under Grafana consuming data from Elasticsearch, DBMS Banks, and monitoring services
- Propose and sponsor improvements in systems development management processes and infrastructure integrations
- Directly involved in the design of the management panel service and its deployment architecture to support decision making
- Team supervision in remote work mode
- Mentoring employees to improve knowledge and career planning using blog content production as an example of a support tool

**Tech Manager NodeJS, Python, DevOps, [Empresa Brasil de Comunicação](http://www.ebc.com.br)** –
Sep/2018 to Oct/2020 – Brasília/DF, Brazil

- Responsible for organizing the development and maintenance of production systems
- Lead systems development team using industry practices for code quality and testing
- Design and support in determining the architecture of systems implemented in Java, Javascript, Python
- Responsible for designing Python systems
- Development of Python application using PostgreSQL or NoSQL/Elasticsearch, and dashboards creation on Kibana or Grafana
- Responsible for designing a computer vision system in Python for content analysis in TV broadcasting
- Directly involved in the analysis and development of a computer vision system in Python for analysis of TV broadcasting content
- Systems development utilizing Javascript, Python, Go Lang languages
- Directly involved in full-stack systems analysis and development referencing best practices and testing/QA methods
- Development of systems tests case supported by tools such as Insomnia, Gitlab/CI, SonarQube, Mocha/ChaiJS, Cypress
- Leadership of development teams concerning CI/CD implementation, tests/QA, automated deployment of systems
- Supervision and support to teams meanwhile implementing the pipeline for the complete application cycle (example: test/QA, release, deploy) and using tools such as Gitlab/CI, Sonatype Nexus, Rancher/Kubernetes, Ansible
- Support the design and development of automation codes to deploy infrastructure (InfraAsCode)
- Management of infrastructure operations automation and development supported by tools such as Ansible, Kubernetes, Helm Chart
- Supervision of operations for Kubernetes as a Service cluster using Rancher and Kubernetes tools
- Team supervision in remote work mode
- Employee mentoring for knowledge improvement and career planning using, for example, Dojo/Kata as a support tool

**Advisor for IT Infrastructure, [Dataprev](http://www.dataprev.gov.br)** –
May/2010-June/2018 – Brasília/DF, Brazil

Dataprev is a governmental company which has 5 development units and owns 3 datacenters in Brazil that develop and provides ICT solutions to support social policies of the Brazilian State.

- Acted as an Influencer in _Agile Infrastructure Implementation_ and _DevOps Practicing_ for the Architecture and the Operations Teams, seeking to improve the quality and delivery time of the infrastructure across all company units.
- Implemented pipeline as code on Gitlab-ci, Jenkins-ci and Docker, increasing applications and puppet module quality.
- Integrally involved to improve process control management, reducing the lead time for changes by 30% and mean time to recover to less than three hours.
- Development of puppet modules improving infrastructure configuration management.
- Worked closely to the Infrastructure Architecture Team to design and implement Rancher/Docker, launching a CaaS service (Container as a Service).
- Guided local and remote teams to implement Data Lake as a Service based on Hadoop framework launching the GovData service for governmental clients.
- Guided local and remote teams in service and software development to create pipelines for tests of applications and modules for infrastructure as code.
- Advice in best practices of security information to implement continuity planning and ICT risk management, best practices for service quality and availability.
- Guided Development Teams to instrument applications, improving the monitoring system via e.g Zabbix and ELK.
- Responsible for external project management in partnership with other institutions from private and public sectors.
- Advisory and support to do a ICT Master Plan.

**IT Manager, [Politec -> Indra Company](http://www.indracompany.com)** – September/2006-April/2010 – Brasília/DF, Brazil

Indra is a leading global consulting and technology company, and the technology partner for the core business operations of its clients' businesses throughout the world.

- Development and implementation of a service for continuous deployment of applications, delivering applications each 30 minutes.
- Designed and implemented a clustered JBoss-4, increasing the availability and performance of the hosted services.
- Maintained Linux (CEntOS/Redhat, Debian) and Windows (2000 and 2003) on virtual-machines on VMware Server.
- Implemented and operated Apache, JBoss-4, IIS, DELL-2950, IBM-236/346/445, DELL-6800 e HP-ML370 systems.
- Advisory in data protection policy, methodology of system development, security information best practices, best practices for quality - and availability of services to different levels of key business and technical stakeholders.
- Management and mentoring of 15 professionals (Operations Team and Level 2-3 Support Analysts).
- Taught classes of configuration and change management of systems and services for the Operations and Development Teams.

### PREVIOUS EXPERIENCES

- **IT Infrastructure Consultant, self-employed** - Feb/2004 - Jan/2007 - Brazil
- **System Engineer, [Aethra Componentes Automotivos](http://www.aethra.com.br)** - Jul/1992 - Oct/2005 - Brazil
- **Developer, [Micro Universo Informática](http://www.microuniverso.com.br)** - Sep/1990 - Jun/1992 - Brazil
- **Developer, Vitória Passos Informática** - Jul/1989 - Aug/1990 - Brazil

# EVENTS

I've had opportunities to conduct forums/conferences/lectures open to the general public, such as:

- **2º DevOpsDays Brasília**, 2017 – member of the organizers team, Brasília/DF, Brazil
- **Open Networking Summit 2017**, 2017 – Panel Discussion: CI/CD Lessons Learned in Open Source Networking - Panelist, Santa Clara/CA, USA
- **1º DevOpsDays Brasília**, 2016 – member of the organizers team; speaker of an open-space _Gitlab-CI+Rancher_, Brasília/DF, Brazil
- **58º Fórum de TIC da Dataprev**, 2014 – Soluções de Big Data – member of organizers team, Brasília/DF, Brazil
- **56º Fórum de TIC da Dataprev**, 2014 – DevOps: Comunicação, colaboração e integração para a entrega contínua e operações estáveis em TIC – member of organizers team, Brasília/DF, Brazil

# AWARDS

- **I Prêmio Ação Coletiva - FISL 9.0** - by Association of Software Free and Ministry of Planning, Budget and Management (Associação do Software Livre e Ministério do Planejamento Orçamento e Gestão)
- **II Prêmio Ação Coletiva - Latinoware 2008** - by Foundation Itaipu Technological Park and the Ministry of Planning, Budget and Management (Fundação Parque Tecnológico Itaipu e Ministério do Planejamento Orçamento e Gestão)
- **III Prêmio Ação Coletiva – Convenção SPB 2009** - by Association of Open Technology (ATA), Intel® and the Brazilian Portal for Public Softwares (ATA-Associação de Tecnologias Abertas (ATA), Intel® e Portal de Software Público Brasileiro)

# OTHER COURSES

- **Big Data 101** - by Cognitive Class (<https://courses.cognitiveclass.ai/certificates/c84511fa6a0d4c3c8284abb562a5518c>) – January/2018
- **Hadoop 101** - by Cognitive Class (<https://courses.cognitiveclass.ai/certificates/a5854b8c96d6486596a2da37771dcbbc>) – January/2018
- **Direito Digital** (Digital law) – by PPP Advogados – 30h – September/2016
- Project Management, COBIT, ITIL, Multi-agent systems, Windows Server, Linux Redhat and SUSE Linux

# CERTIFICATIONS

- **ITIL Foundation** – by Prometric/EXIN, registry \#: SA 7LAM 5005

# LANGUAGES

**English**: Advanced | **Spanish**: Basic | **Portuguese**: Native

# ACADEMIC STUDY

- 2004 **MBA Business Management** – Fundação Getúlio Vargas, Brazil
- 2000 **Mechanical Engineering** – Pontifícia Universidade Católica de Minas Gerais, Brazil

# **OTHER ACTIVITIES AND VOLUNTEERING**

- [CACIC project](https://softwarepublico.gov.br/social/cacic) - _open-source agent based inventory system to make the diagnosis of the computational park_, Lead Developer – April/2007 to April/2010, Brazil
- Social Project of a neighborhood church for social and digital inclusion – Member of the Volunteers Team – 2005/2006 – Camargos/BH, Brazil
- lectures, taught courses and training for managers, leaders, analysts and the general public – 1995/2004; ERP modules (MRP, finance, accounting etc) – 1993/2004; Working in group and its tools – 2004
- Author of article: [Espírito Livre magazine](http://www.revista.espiritolivre.org), ed 10, available at: <http://tiny.cc/EspiritoLivreMagazine>
- Code sharing and contributions:
  - Gitlab: <https://gitlab.com/adrianovieira>
  - Github: <https://github.com/adrianovieira>
  - Vagrant Boxes: <https://app.vagrantup.com/adrianovieira>; <https://app.vagrantup.com/entlinux>
  - Docker images: <https://hub.docker.com/r/adrianovieira/>
  - Sourceforge (as harpiain): <https://sourceforge.net/u/harpiain>
  - Software Público Brasileiro (as harpiain): <https://softwarepublico.gov.br/gitlab/cacic>
  - Speakerdeck: <https://speakerdeck.com/adrianovieira>
  - Bitbucket: [https://bitbucket.org/adriano_svieira/](https://bitbucket.org/adriano_svieira/)
  - blog: <https://adrianovieira.gitlab.io>
