# My Curriculum Vitae - Résumé

My CV is originally written in *markdown*[^markdown].

[^markdown]: Markdown a markup language [https://en.wikipedia.org/wiki/Markdown](https://en.wikipedia.org/wiki/Markdown)

I've wrote it in the languages below:

- Portuguese
- English

It also is auto-generated in ***PDF***  format on this repository via it's own CI which I set it up. The last ones built can be downloaded from the link below:

[![pipeline status](https://gitlab.com/adrianovieira/cv-resume/badges/main/pipeline.svg)](https://gitlab.com/adrianovieira/cv-resume/-/jobs/artifacts/master/browse?job=build::cv)  https://gitlab.com/adrianovieira/cv-resume/-/jobs/artifacts/main/browse?job=build::cv

Fortunately, one also can generate a PDF manually...

- using docker:
```bash
docker run -it --rm -v $(pwd):/build -w /build \
           registry.gitlab.com/adrianovieira/containerize/pandoc:f28 \
             pandoc --template=config/template.tex config/template-metadata.yml \
                    config/mydata-metadata.yml Portuguese.md \
                    -o AdrianoVieira-Portuguese-cv.pdf
```

- joining PDF (`GPL Ghostscript 9.23`):
```bash
docker run -it --rm -v $(pwd):/build -w /build \
           registry.gitlab.com/adrianovieira/containerize/pandoc:f28 \
             gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite \
                -sOutputFile=AdrianoVieira-clcv.pdf \
                AdrianoVieira-Portuguese-cl.pdf AdrianoVieira-Portuguese-cv.pdf
```

Alternativelly, what if you take a look on my *Linkedin* page at https://www.linkedin.com/in/adriano-svieira ...
